﻿using System.Net;
using System.Text.Json.Serialization;
using Mango.Web.Models;
using Mango.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Mango.Web.Controllers;

public class CartController : Controller
{
    private readonly IProductService _productService;
    private readonly ICartService _cartService;
    private readonly ICouponService _couponService;


    public CartController(
        ICartService cartService, 
        IProductService productService, 
        ICouponService couponService)
    {
        _cartService = cartService;
        _productService = productService;
        _couponService = couponService;
    }

    // GET
    public async Task<IActionResult> Index()
    {
        return View(await LoadCartDtoBaseOnLoggedInUser());
    }
    
    public async Task<IActionResult> Checkout()
    {
        return View(await LoadCartDtoBaseOnLoggedInUser());
    }
    
    [HttpPost]
    public async Task<IActionResult> Checkout(CartDto cartDto)
    {
        try
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            var response = await _cartService.Checkout<ResponseDto>(accessToken, cartDto.CartHeader);

            if (!response.IsSuccess)
            {
                TempData["Error"] = response.DisplayMessage;
                return RedirectToAction(nameof(Checkout));
            }
            
            return RedirectToAction(nameof(Confirmation));
        }
        catch
        {
            return View(cartDto);
        }
    }
    
    public async Task<IActionResult> Confirmation()
    {
        return View();
    }
    
    public async Task<IActionResult> Remove(int cartDetailsId)
    {
        var accessToken = await HttpContext.GetTokenAsync("access_token");

        var response = await _cartService.RemoveFromCart<ResponseDto>(accessToken, cartDetailsId);
        if (response?.IsSuccess ?? false)
        {
            return RedirectToAction(nameof(Index));
        }

        //TODO: add validation message
        return RedirectToAction(nameof(Index));
    }

    [HttpPost]
    public async Task<IActionResult> ApplyCoupon(CartDto cartDto)
    {
        var accessToken = await HttpContext.GetTokenAsync("access_token");

        var response = await _cartService.ApplyCoupon<ResponseDto>(accessToken, cartDto);
        if (response?.IsSuccess ?? false)
        {
            return RedirectToAction(nameof(Index));
        }

        //TODO: add validation message
        return RedirectToAction(nameof(Index));
    }
    
    [HttpPost]
    public async Task<IActionResult> RemoveCoupon(CartDto cartDto)
    {
        var accessToken = await HttpContext.GetTokenAsync("access_token");

        var response = await _cartService.RemoveCoupon<ResponseDto>(accessToken, cartDto.CartHeader.UserId);
        if (response?.IsSuccess ?? false)
        {
            return RedirectToAction(nameof(Index));
        }

        //TODO: add validation message
        return RedirectToAction(nameof(Index));
    }

    private async Task<CartDto> LoadCartDtoBaseOnLoggedInUser()
    {
        var userId = User.Claims.FirstOrDefault(x => x.Type == "sub")?.Value;
        var accessToken = await HttpContext.GetTokenAsync("access_token");

        var response = await _cartService.GetCartByUserId<ResponseDto>(accessToken, userId);

        var cart = new CartDto();
        
        if (response?.IsSuccess ?? false)
        {
            cart = JsonConvert.DeserializeObject<CartDto>(Convert.ToString(response.Result));
        }

        if (cart.CartHeader != null)
        {
            foreach (var cartDetails in cart.CartDetails)
            {
                cart.CartHeader.OrderTotal += cartDetails.Count * cartDetails.Product.Price;
            }

            if (!string.IsNullOrEmpty(cart.CartHeader.CouponCode))
            {
                var couponResponse = await _couponService
                    .GetCoupon<ResponseDto>(accessToken, cart.CartHeader.CouponCode);
                
                if (couponResponse?.IsSuccess ?? false)
                {
                    var coupon = JsonConvert.DeserializeObject<CouponDto>(Convert.ToString(couponResponse.Result));
                    cart.CartHeader.DiscountTotal = coupon.DiscountAmount;
                }

                cart.CartHeader.OrderTotal -= cart.CartHeader.DiscountTotal;
            }
        }

        return cart;
    }
}
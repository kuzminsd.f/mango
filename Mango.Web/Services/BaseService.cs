﻿using System.Net.Http.Headers;
using System.Text;
using System.Text.Json.Serialization;
using Mango.Web.Models;
using Mango.Web.Services.Interfaces;
using Newtonsoft.Json;
using JsonConverter = System.Text.Json.Serialization.JsonConverter;

namespace Mango.Web.Services;

public class BaseService : IBaseService
{
    public ResponseDto ResponseModel { get; set; }
    protected IHttpClientFactory HttpClient { get; set; }

    public BaseService(IHttpClientFactory httpClient)
    {
        ResponseModel = new ResponseDto();
        HttpClient = httpClient;
    }

    public async Task<T?> Send<T>(ApiRequest request) 
    {
        try
        {
            var client = HttpClient.CreateClient("MangoApi");
            client.DefaultRequestHeaders.Clear();

            var requestMessage = new HttpRequestMessage();
            requestMessage.Headers.Add("Accept", "application/json");
            requestMessage.RequestUri = new Uri(request.Url);

            if (request.Data != null)
            {
                requestMessage.Content = new StringContent(
                    JsonConvert.SerializeObject(request.Data),
                    Encoding.UTF8,
                    "application/json");
            }

            if (!string.IsNullOrEmpty(request.AccessToken))
            {
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", request.AccessToken);
            }

            switch (request.ApiType)
            {
                case Settings.ApiType.Post:
                    requestMessage.Method = HttpMethod.Post;
                    break;
                case Settings.ApiType.Put:
                    requestMessage.Method = HttpMethod.Put;
                    break;
                case Settings.ApiType.Delete:
                    requestMessage.Method = HttpMethod.Delete;
                    break;
                default:
                    requestMessage.Method = HttpMethod.Get;
                    break;
            }

            var responseMessage = await client.SendAsync(requestMessage);
            var content = await responseMessage.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }
        catch (Exception ex)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(new ResponseDto()
            {
                DisplayMessage = "Error",
                ErrorMessages = new List<string>() { ex.Message },
                IsSuccess = false
            }));
        }
    }
    
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
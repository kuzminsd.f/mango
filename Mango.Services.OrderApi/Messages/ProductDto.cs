﻿namespace Mango.Services.OrderApi.Messages;

public class ProductDto
{
    public int ProductId { get; set; }

    public string Name { get; set; } = default!;

    public double Price { get; set; }
    
    public string? Description { get; set; }

    public string CategoryName { get; set; } = default!;
    
    public string? ImageUrl { get; set; }
}
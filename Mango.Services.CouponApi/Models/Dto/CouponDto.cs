﻿namespace Mango.Services.CouponApi.Models.Dto;

public class CouponDto
{
    public int CouponId { get; set; }
    
    public string CouponCode { get; set; } = default!;
    
    public double DiscountAmount { get; set; }
    
    public bool FixedAmount { get; set; }
    
    public DateTime ExpirationDate { get; set; }
    
    public bool IsDeleted { get; set; }
}
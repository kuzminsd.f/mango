﻿using Mango.Web.Models;
using Mango.Web.Services.Interfaces;

namespace Mango.Web.Services;

public class ProductService : BaseService, IProductService
{
    public ProductService(IHttpClientFactory httpClient) : base(httpClient)
    {
    }

    public async Task<T> GetAllProducts<T>(string token)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Get,
            Url = $"{Settings.ProductApiBase}/api/products",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> GetProductById<T>(string token, int id)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Get,
            Url = $"{Settings.ProductApiBase}/api/products/{id}",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> CreateProduct<T>(string token, ProductDto product)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Post,
            Data = product,
            Url = $"{Settings.ProductApiBase}/api/products",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> UpdateProduct<T>(string token, ProductDto product)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Put,
            Data = product,
            Url = $"{Settings.ProductApiBase}/api/products",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> DeleteProduct<T>(string token, int id)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Delete,
            Url = $"{Settings.ProductApiBase}/api/products/{id}",
            AccessToken = token
        });

        return result;
    }
}
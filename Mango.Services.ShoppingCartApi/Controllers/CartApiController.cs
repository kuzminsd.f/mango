﻿using Mango.MessageBus;
using Mango.Services.ShoppingCartApi.Messages;
using Mango.Services.ShoppingCartApi.Models.Dto;
using Mango.Services.ShoppingCartApi.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Mango.Services.ShoppingCartApi.Controllers;

[ApiController]
[Route("api/cart")]
public class CartController : Controller
{
    private readonly ICartRepository _cartRepository;
    private readonly ICouponRepository _couponRepository;
    private readonly ResponseDto _responseDto;
    private readonly IMessageBus _messageBus;

    // GET
    public CartController(
        ICartRepository cartRepository, 
        IMessageBus messageBus, 
        ICouponRepository couponRepository)
    {
        _cartRepository = cartRepository;
        _messageBus = messageBus;
        _couponRepository = couponRepository;
        _responseDto = new ResponseDto();
    }

    [HttpGet("GetCart/{userId}")]
    public async Task<object> GetCart(string userId)
    {
        try
        {
            var cart = await _cartRepository.GetCartByUserId(userId);
            _responseDto.Result = cart;
        }
        catch (Exception ex)
        {
            _responseDto.IsSuccess = false;
            _responseDto.ErrorMessages = new List<string>() { ex.Message };
        }

        return _responseDto;
    }
    
    [HttpPost("AddCart")]
    public async Task<object> AddCart(CartDto cartDto)
    {
        try
        {
            var cart = await _cartRepository.CreateUpdateCart(cartDto);
            _responseDto.Result = cart;
        }
        catch (Exception ex)
        {
            _responseDto.IsSuccess = false;
            _responseDto.ErrorMessages = new List<string>() { ex.Message };
        }

        return _responseDto;
    }
    
    [HttpPost("UpdateCart")]
    public async Task<object> UpdateCart(CartDto cartDto)
    {
        try
        {
            var cart = await _cartRepository.CreateUpdateCart(cartDto);
            _responseDto.Result = cart;
        }
        catch (Exception ex)
        {
            _responseDto.IsSuccess = false;
            _responseDto.ErrorMessages = new List<string>() { ex.Message };
        }

        return _responseDto;
    }
    
    [HttpPost("RemoveCart")]
    public async Task<object> RemoveCart([FromBody] int cartId)
    {
        try
        {
            var isSuccess = await _cartRepository.RemoveFromCart(cartId);
            _responseDto.Result = isSuccess;
        }
        catch (Exception ex)
        {
            _responseDto.IsSuccess = false;
            _responseDto.ErrorMessages = new List<string>() { ex.Message };
        }

        return _responseDto;
    }
    
    [HttpPost("ClearCart/{userId}")]
    public async Task<object> ClearCart([FromBody] string userId)
    {
        try
        {
            var isSuccess = await _cartRepository.ClearCart(userId);
            _responseDto.Result = isSuccess;
        }
        catch (Exception ex)
        {
            _responseDto.IsSuccess = false;
            _responseDto.ErrorMessages = new List<string>() { ex.Message };
        }

        return _responseDto;
    }
    
    [HttpPost("ApplyCoupon")]
    public async Task<object> ApplyCoupon([FromBody] CartDto cart)
    {
        try
        {
            var isSuccess = await _cartRepository.ApplyCoupon(cart.CartHeader.UserId, cart.CartHeader.CouponCode);
            _responseDto.Result = isSuccess;
        }
        catch (Exception ex)
        {
            _responseDto.IsSuccess = false;
            _responseDto.ErrorMessages = new List<string>() { ex.Message };
        }

        return _responseDto;
    }
    
    [HttpPost("RemoveCoupon")]
    public async Task<object> RemoveCoupon([FromBody] string userId)
    {
        try
        {
            var isSuccess = await _cartRepository.RemoveCoupon(userId);
            _responseDto.Result = isSuccess;
        }
        catch (Exception ex)
        {
            _responseDto.IsSuccess = false;
            _responseDto.ErrorMessages = new List<string>() { ex.Message };
        }

        return _responseDto;
    }
    
    [HttpPost("Checkout")]
    public async Task<object> Checkout([FromBody]CheckoutHeaderDto checkoutHeaderDto)
    {
        try
        {
            var cartDto = await _cartRepository.GetCartByUserId(checkoutHeaderDto.UserId);

            if (cartDto is null)
            {
                return BadRequest();
            }

            if (!string.IsNullOrEmpty(checkoutHeaderDto.CouponCode))
            {
                var coupon = await _couponRepository.GetCouponByCode(checkoutHeaderDto.CouponCode);

                if (coupon.DiscountAmount != checkoutHeaderDto.DiscountTotal)
                {
                    _responseDto.IsSuccess = false;
                    _responseDto.ErrorMessages = new List<string>() { "Coupon Price has been changed, please confirm" };
                    _responseDto.DisplayMessage = "Coupon Price has been changed, please confirm";
                    return _responseDto;
                }
            }

            checkoutHeaderDto.CartDetails = cartDto.CartDetails;
            checkoutHeaderDto.Id = checkoutHeaderDto.CartHeaderId;
            await _messageBus.PublishMessage(checkoutHeaderDto, KafkaTopics.CheckoutMessageTopic);
            await _cartRepository.ClearCart(checkoutHeaderDto.UserId);
            
            _responseDto.Result = true;
        }
        catch (Exception ex)
        {
            _responseDto.IsSuccess = false;
            _responseDto.ErrorMessages = new List<string>() { ex.Message };
        }

        return _responseDto;
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mango.Services.ShoppingCartApi.Models;

public class Product
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int ProductId { get; set; }

    [Required]
    public string Name { get; set; } = default!;
    
    [Range(1, 1000)]
    public double Price { get; set; }
    
    public string? Description { get; set; }

    [Required]
    public string CategoryName { get; set; } = default!;
    
    public string? ImageUrl { get; set; }
}
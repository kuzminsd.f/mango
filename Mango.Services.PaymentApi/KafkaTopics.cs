﻿namespace Mango.Services.PaymentApi;

public static class KafkaTopics
{
    public const string OrderPaymentProcessTopic = "order_payment_process";
    public const string OrderUpdatePaymentResultTopic = "order_update_payment_result";

}
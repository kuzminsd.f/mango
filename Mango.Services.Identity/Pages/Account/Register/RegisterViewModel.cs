﻿using System.ComponentModel.DataAnnotations;
using Mango.Services.Identity.Pages.Login;

namespace Mango.Services.Identity.Pages.Register;

public class RegisterViewModel
{
    [Required]
    public string Username { get; set; }

    [Required]
    public string Email { get; set; }

    public string FirstName { get; set; }
    public string LastName { get; set; }

    [Required]
    public string Password { get; set; }

    public string ReturnUrl { get; set; }
    public string RoleName { get; set; }

    public bool AllowRememberLogin { get; set; } = true;
    public bool EnableLocalLogin { get; set; } = true;

    public IEnumerable<ViewModel.ExternalProvider> ExternalProviders { get; set; } =
        Enumerable.Empty<ViewModel.ExternalProvider>();

    public IEnumerable<ViewModel.ExternalProvider> VisibleExternalProviders =>
        ExternalProviders.Where(x => !String.IsNullOrWhiteSpace(x.DisplayName));

    public bool IsExternalLoginOnly => EnableLocalLogin == false && ExternalProviders?.Count() == 1;

    public string ExternalLoginScheme =>
        IsExternalLoginOnly ? ExternalProviders?.SingleOrDefault()?.AuthenticationScheme : null;
}
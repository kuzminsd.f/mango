﻿using Mango.Services.ShoppingCartApi.Models.Dto;
using Newtonsoft.Json;

namespace Mango.Services.ShoppingCartApi.Repository;

public class CouponRepository : ICouponRepository
{
    private readonly HttpClient _httpClient;

    public CouponRepository(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<CouponDto> GetCouponByCode(string code)
    {
        var response = await _httpClient.GetAsync($"{Settings.CouponApiBase}/api/coupon/{code}");
        var apiContent = await response.Content.ReadAsStringAsync();
        var responseDto = JsonConvert.DeserializeObject<ResponseDto>(apiContent);

        if (responseDto?.IsSuccess ?? false)
        {
            return JsonConvert.DeserializeObject<CouponDto>(Convert.ToString(responseDto.Result));
        }

        return new CouponDto();
    }
}
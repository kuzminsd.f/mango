﻿using Mango.Services.ShoppingCartApi.Models.Dto;

namespace Mango.Services.ShoppingCartApi.Repository;

public interface ICouponRepository
{
    Task<CouponDto> GetCouponByCode(string code);
}
﻿namespace Mango.MessageBus;

public interface IMessageBus
{
    Task PublishMessage<T>(T message, string topicName) where T : BaseMessage, new();
}
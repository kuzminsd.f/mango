﻿using Confluent.Kafka;

namespace Mango.MessageBus;

public class KafkaMessageBus : IMessageBus
{
    private readonly ProducerConfig _producerConfig;

    public KafkaMessageBus(KafkaOptions kafkaOptions)
    {
        _producerConfig = new ProducerConfig
        {
            BootstrapServers = kafkaOptions.BootstrapServers,
            Acks = Acks.All
        };
    }
    
    public async Task PublishMessage<T>(T message, string topicName) where T : BaseMessage, new()
    {
        using var producer = new ProducerBuilder<long, T>(_producerConfig)
            .SetValueSerializer(new JsonSerializer<T>())
            .Build();
        
        await producer.ProduceAsync(
            topicName,
            new Message<long, T>()
            {
                Key = message.Id,
                Value = message
            });
    }
}
﻿namespace Mango.Infrastructure;

/// <summary>
/// 
/// </summary>
public class Response
{
    public bool IsSuccess { get; set; } = true;
    public object? Result { get; set; }
    public string? DisplayMessage { get; set; }
}
﻿using Mango.Services.CouponApi.Models;

namespace Mango.Services.CouponApi.Repository;

public interface ICouponRepository
{
    Task<Coupon?> GetByCode(string couponCode, CancellationToken cancellationToken);
    Task<Coupon?> GetById(int couponId, CancellationToken cancellationToken);
    Task<IEnumerable<Coupon>> GetAll(CancellationToken cancellationToken);
    Task<Coupon> Create(Coupon coupon, CancellationToken cancellationToken);
    Task<Coupon> Update(Coupon coupon, CancellationToken cancellationToken);
}
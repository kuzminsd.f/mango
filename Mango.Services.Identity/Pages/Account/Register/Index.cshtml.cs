﻿using System.Security.Claims;
using Duende.IdentityServer.Events;
using Duende.IdentityServer.Services;
using Duende.IdentityServer.Stores;
using IdentityModel;
using Mango.Services.Identity.Models;
using Mango.Services.Identity.Pages.Login;
using Mango.Services.Identity.Pages.Register;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

//namespace Mango.Services.Identity.Pages.Account.Register;
namespace Mango.Services.Identity.Pages.Register;

[AllowAnonymous]
public class Index : PageModel
{
    private readonly IIdentityServerInteractionService _interaction;
    private readonly IAuthenticationSchemeProvider _schemeProvider;
    private readonly IEventService _events;
    private readonly UserManager<ApplicationUser> _users;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly IClientStore _clientStore;
    
    [BindProperty]
    public RegisterViewModel Input { get; set; }

    public Index(
        SignInManager<ApplicationUser> signInManager, 
        RoleManager<IdentityRole> roleManager, 
        UserManager<ApplicationUser> users, 
        IEventService events, 
        IAuthenticationSchemeProvider schemeProvider, 
        IIdentityServerInteractionService interaction, IClientStore clientStore)
    {
        _signInManager = signInManager;
        _roleManager = roleManager;
        _users = users;
        _events = events;
        _schemeProvider = schemeProvider;
        _interaction = interaction;
        _clientStore = clientStore;
    }

    public async Task<IActionResult> OnGet(string returnUrl)
    {
        await BuildRegisterViewModelAsync(returnUrl);
        return Page();
    }
    
    public async Task<IActionResult> OnPost()
    {
        if (ModelState.IsValid)
        {

            var user = new ApplicationUser
            {
                UserName = Input.Username,
                Email = Input.Email,
                EmailConfirmed = true,
                FirstName = Input.FirstName,
                LastName = Input.LastName
            };

            var result = await _users.CreateAsync(user, Input.Password);
            if (result.Succeeded)
            {
                if (!_roleManager.RoleExistsAsync(Input.RoleName).GetAwaiter().GetResult())
                {
                    var userRole = new IdentityRole
                    {
                        Name = Input.RoleName,
                        NormalizedName = Input.RoleName,

                    };
                    await _roleManager.CreateAsync(userRole);
                }

                await _users.AddToRoleAsync(user, Input.RoleName);

                await _users.AddClaimsAsync(user, new Claim[]
                {
                    new Claim(JwtClaimTypes.Name, Input.Username),
                    new Claim(JwtClaimTypes.Email, Input.Email),
                    new Claim(JwtClaimTypes.GivenName, Input.FirstName),
                    new Claim(JwtClaimTypes.FamilyName, Input.LastName),
                    new Claim(JwtClaimTypes.WebSite, "http://" + Input.Username + ".com"),
                    new Claim(JwtClaimTypes.Role, "User")
                });

                var context = await _interaction.GetAuthorizationContextAsync(Input.ReturnUrl);
                var loginresult = await _signInManager.PasswordSignInAsync(Input.Username, Input.Password, false,
                    lockoutOnFailure: true);
                if (loginresult.Succeeded)
                {
                    var checkuser = await _users.FindByNameAsync(Input.Username);
                    await _events.RaiseAsync(new UserLoginSuccessEvent(checkuser.UserName, checkuser.Id,
                        checkuser.UserName, clientId: context?.Client.ClientId));

                    if (context != null)
                    {
                        if (context.IsNativeClient())
                        {
                            // The client is native, so this change in how to
                            // return the response is for better UX for the end user.
                            return this.LoadingPage(Input.ReturnUrl);
                        }

                        // we can trust model.ReturnUrl since GetAuthorizationContextAsync returned non-null
                        return Redirect(Input.ReturnUrl);
                    }

                    // request for a local page
                    if (Url.IsLocalUrl(Input.ReturnUrl))
                    {
                        return Redirect(Input.ReturnUrl);
                    }
                    else if (string.IsNullOrEmpty(Input.ReturnUrl))
                    {
                        return Redirect("~/");
                    }
                    else
                    {
                        // user might have clicked on a malicious link - should be logged
                        throw new Exception("invalid return URL");
                    }
                }

            }
        }

        // If we got this far, something failed, redisplay form
        return Page();
    }

    private async Task BuildRegisterViewModelAsync(string returnUrl)
    {
        var context = await _interaction.GetAuthorizationContextAsync(returnUrl);
        var roles = new List<string>
        {
            "Admin",
            "Customer"
        };

        ViewData["message"] = roles;
        if (context?.IdP != null && await _schemeProvider.GetSchemeAsync(context.IdP) != null)
        {
            var local = context.IdP == Duende.IdentityServer.IdentityServerConstants.LocalIdentityProvider;

            // this is meant to short circuit the UI and only trigger the one external IdP
            var vm = new RegisterViewModel
            {
                EnableLocalLogin = local,
                ReturnUrl = returnUrl,
                Username = context?.LoginHint,
            };

            if (!local)
            {
                vm.ExternalProviders = new[] { new ViewModel.ExternalProvider { AuthenticationScheme = context.IdP } };
            }

            Input = vm;
            return;
        }

        var schemes = await _schemeProvider.GetAllSchemesAsync();

        var providers = schemes
            .Where(x => x.DisplayName != null)
            .Select(x => new ViewModel.ExternalProvider
            {
                DisplayName = x.DisplayName ?? x.Name,
                AuthenticationScheme = x.Name
            }).ToList();

        var allowLocal = true;
        if (context?.Client.ClientId != null)
        {
            var client = await _clientStore.FindEnabledClientByIdAsync(context.Client.ClientId);
            if (client != null)
            {
                allowLocal = client.EnableLocalLogin;

                if (client.IdentityProviderRestrictions != null && client.IdentityProviderRestrictions.Any())
                {
                    providers = providers.Where(provider =>
                        client.IdentityProviderRestrictions.Contains(provider.AuthenticationScheme)).ToList();
                }
            }
        }

        Input = new RegisterViewModel
        {
            AllowRememberLogin = LoginOptions.AllowRememberLogin,
            EnableLocalLogin = allowLocal && LoginOptions.AllowLocalLogin,
            ReturnUrl = returnUrl,
            Username = context?.LoginHint,
            ExternalProviders = providers.ToArray()
        };
    }
}
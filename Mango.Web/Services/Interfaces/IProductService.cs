﻿using Mango.Web.Models;

namespace Mango.Web.Services.Interfaces;

public interface IProductService : IBaseService
{
    Task<T> GetAllProducts<T>(string token);
    Task<T> GetProductById<T>(string token, int id);
    Task<T> CreateProduct<T>(string token, ProductDto product);
    Task<T> UpdateProduct<T>(string token, ProductDto product);
    Task<T> DeleteProduct<T>(string token, int id);
}
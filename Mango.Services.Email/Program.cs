using Mango.MessageBus;
using Mango.Services.Email.Messaging;
using Mango.Services.Email.Repository;
using Mango.Services.OrderApi.DbContexts;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"));
});

var kafkaOptions = builder.Configuration.GetSection(nameof(KafkaOptions)).Get<KafkaOptions>();

builder.Services.AddSingleton(kafkaOptions);
builder.Services.AddScoped<IEmailRepository, EmailRepository>();
builder.Services.AddHostedService<EmailServiceConsumer>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
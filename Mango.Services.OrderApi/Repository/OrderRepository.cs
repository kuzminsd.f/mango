﻿using Mango.Services.OrderApi.DbContexts;
using Mango.Services.OrderApi.Models;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.OrderApi.Repository;

public class OrderRepository : IOrderRepository
{
    private readonly ApplicationDbContext _dbContext;

    public OrderRepository(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<bool> AddOrder(OrderHeader orderHeader)
    {
        _dbContext.OrderHeaders.Add(orderHeader);
        await _dbContext.SaveChangesAsync();
        return true;
    }

    public async Task<bool> UpdateOrderPaymentStatus(int orderHeaderId, bool paid)
    {
        try
        {
            var orderHeader = await _dbContext
                .OrderHeaders
                .FirstAsync(x => x.OrderHeaderId == orderHeaderId);

            orderHeader.PaymentStatus = paid;
            await _dbContext.SaveChangesAsync();

            return true;
        }
        catch
        {
            return false;
        }
    }
}
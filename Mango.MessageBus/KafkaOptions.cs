﻿namespace Mango.MessageBus;

public class KafkaOptions
{
    public string BootstrapServers { get; init; } = string.Empty;
}
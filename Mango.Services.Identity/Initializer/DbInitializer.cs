﻿using System.Security.Claims;
using IdentityModel;
using Mango.Services.Identity.DbContexts;
using Mango.Services.Identity.Models;
using Microsoft.AspNetCore.Identity;

namespace Mango.Services.Identity.Initializer;

public class DbInitializer : IDbInitializer
{
    private readonly ApplicationDbContext _dbContext;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;

    public DbInitializer(
        ApplicationDbContext dbContext, 
        UserManager<ApplicationUser> userManager, 
        RoleManager<IdentityRole> roleManager)
    {
        _dbContext = dbContext;
        _userManager = userManager;
        _roleManager = roleManager;
    }
    
    public async Task Initialize()
    {
        var adminRole = await _roleManager.FindByNameAsync(Settings.Admin);

        if (adminRole != null)
        {
            return;
        }
        
        await _roleManager.CreateAsync(new IdentityRole(Settings.Admin));
        await _roleManager.CreateAsync(new IdentityRole(Settings.Customer));

        var adminUser = new ApplicationUser()
        {
            UserName = "admin1@gmail.com",
            Email = "admin1@gmail.com",
            EmailConfirmed = true,
            PhoneNumber = "+77777777777",
            FirstName = "Ben",
            LastName = "Admin"
        };

        await _userManager.CreateAsync(adminUser, "Admin123*");
        await _userManager.AddToRoleAsync(adminUser, Settings.Admin);

        var adminIdentityResult = await _userManager.AddClaimsAsync(adminUser, new[]
        {
            new Claim(JwtClaimTypes.Name, $"{adminUser.FirstName} {adminUser.LastName}"),
            new Claim(JwtClaimTypes.GivenName, adminUser.FirstName),
            new Claim(JwtClaimTypes.FamilyName, adminUser.LastName),
            new Claim(JwtClaimTypes.Role, Settings.Admin),
        });
        
        var customerUser = new ApplicationUser()
        {
            UserName = "customer1@gmail.com",
            Email = "customer1@gmail.com",
            EmailConfirmed = true,
            PhoneNumber = "+77777777777",
            FirstName = "Paul",
            LastName = "Customer"
        };

        await _userManager.CreateAsync(customerUser, "Customer123*");
        await _userManager.AddToRoleAsync(customerUser, Settings.Customer);

        var customerIdentityResult = await _userManager.AddClaimsAsync(customerUser, new[]
        {
            new Claim(JwtClaimTypes.Name, $"{customerUser.FirstName} {customerUser.LastName}"),
            new Claim(JwtClaimTypes.GivenName, customerUser.FirstName),
            new Claim(JwtClaimTypes.FamilyName, customerUser.LastName),
            new Claim(JwtClaimTypes.Role, Settings.Customer),
        });
    }
}
﻿using Mango.Web.Models;
using Mango.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using JsonConverter = System.Text.Json.Serialization.JsonConverter;

namespace Mango.Web.Controllers;

public class ProductController : Controller
{
    private readonly IProductService _productService;

    public ProductController(IProductService productService)
    {
        _productService = productService;
    }

    
    public async Task<IActionResult> Index()
    {
        var accessToken = await HttpContext.GetTokenAsync("access_token");
        var response = await _productService.GetAllProducts<ResponseDto>(accessToken);

        if (response.IsSuccess && response.Result != null)
        {
            var products = JsonConvert.DeserializeObject<List<ProductDto>>(response.Result.ToString());
            return View(products);
        }

        throw new NotImplementedException("Exception handler is not implemented yet");
    }
    
    public async Task<IActionResult> ProductCreate()
    {
        return View();
    }
    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> ProductCreate(ProductDto model)
    {
        if (ModelState.IsValid)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            var response = await _productService.CreateProduct<ResponseDto>(accessToken, model);

            if (response.IsSuccess && response.Result != null)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        throw new NotImplementedException("Exception handler is not implemented yet");
    }
    
    public async Task<IActionResult> ProductEdit(int productId)
    {
        var accessToken = await HttpContext.GetTokenAsync("access_token");
        var response = await _productService.GetProductById<ResponseDto>(accessToken, productId);

        if (response.IsSuccess && response.Result != null)
        {
            return View(JsonConvert.DeserializeObject<ProductDto>(response.Result.ToString()));
        }

        return NotFound();
    }
    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> ProductEdit(ProductDto model)
    {
        if (ModelState.IsValid)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            var response = await _productService.UpdateProduct<ResponseDto>(accessToken, model);

            if (response.IsSuccess && response.Result != null)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        throw new NotImplementedException("Exception handler is not implemented yet");
    }
    
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> ProductDelete(int productId)
    {
        var accessToken = await HttpContext.GetTokenAsync("access_token");
        var response = await _productService.GetProductById<ResponseDto>(accessToken, productId);

        if (response.IsSuccess && response.Result != null)
        {
            return View(JsonConvert.DeserializeObject<ProductDto>(response.Result.ToString()));
        }

        return NotFound();
    }
    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> ProductDelete(ProductDto model)
    {
        if (ModelState.IsValid)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            var response = await _productService.DeleteProduct<ResponseDto>(accessToken, model.ProductId);

            if (response.IsSuccess && response.Result != null)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        throw new NotImplementedException("Exception handler is not implemented yet");
    }
}
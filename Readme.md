# Readme
## Welcome
Hello, my name is Sergey and I recently took a course on microservices. As part of this course, I made this project - a web application for a store where you can order some goods. I'm going to expand on the current functionality, continue refactoring the code, and do some experimentation with it.

## About the project

This project was completed as part of the following course:
https://www.udemy.com/course/net-core-microservices-net-mvc/

You can see the source code here: https://github.com/bhrugen/Mango

I did not agree with some of the author's decisions, so I implemented some parts differently.
The most important changed parts:
- I've used IHosted service instead of using extensions
- I've used kafka instead of using Azure Messaging Bus
- I've changed some naming to make code cleaner
- MS SQL Server was replaced with PostgreSQL

## Next steps:

 - Add documentation (functional requirements, use cases, ubiquitous language, bounded contexts)
 - Add a migration configuration to the solution (to migrate all projects at the same time)
 - Add link to task manager 
 - Do some refactoring the current code
 - Extend functionality in accordance with functional requirements


## TODO:
### Coupon Api Service:

 - Replace http with GRPC protocol
﻿using Google.Protobuf;

namespace Mango.MessageBus;

public class BaseMessage
{
    public long Id { get; set; }
    public DateTime MessageCreated { get; set; }
}
﻿namespace Mango.Services.ShoppingCartApi;

public static class KafkaTopics
{
    public const string CheckoutMessageTopic = "checkout_message";
}
﻿using Mango.Services.OrderApi.Models;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.OrderApi.DbContexts;

public class ApplicationDbContext : DbContext
{
    public DbSet<OrderHeader> OrderHeaders { get; set; } = default!;

    public DbSet<OrderDetails> OrderDetails { get; set; } = default!;

    
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) 
        : base(options)
    {
        
    }
}
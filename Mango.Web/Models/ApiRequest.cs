﻿using static Mango.Web.Settings;

namespace Mango.Web.Models;

public class ApiRequest
{
    public ApiType ApiType { get; set; } = ApiType.Get;
    
    public string Url { get; set; } = default!;
    
    public object? Data { get; set; }
    
    public string AccessToken { get; set; } = default!;
}
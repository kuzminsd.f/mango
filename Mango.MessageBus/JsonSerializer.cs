﻿using System.Text.Json;
using Confluent.Kafka;

namespace Mango.MessageBus;

public class JsonSerializer<T> : ISerializer<T>, IDeserializer<T> where T : new()
{
    public byte[] Serialize(T data, SerializationContext context)
    {
        using var ms = new MemoryStream();
        var jsonString = JsonSerializer.Serialize(data);
        var writer = new StreamWriter(ms);

        writer.Write(jsonString);
        writer.Flush();
        ms.Position = 0;

        return ms.ToArray();
    }

    public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
    {
        return JsonSerializer.Deserialize<T>(data.ToArray()) ?? throw new InvalidOperationException();
    }
}
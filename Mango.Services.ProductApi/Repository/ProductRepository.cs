﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Mango.Services.ProductApi.DbContexts;
using Mango.Services.ProductApi.Models;
using Mango.Services.ProductApi.Models.Dto;

namespace Mango.Services.ProductApi.Repository;

public class ProductRepository : IProductRepository
{
    private ApplicationDbContext _dbContext;
    private IMapper _mapper;
    
    public ProductRepository(
        ApplicationDbContext dbContext, 
        IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    //TODO: Convert to IAsyncEnumerable
    public async Task<IEnumerable<ProductDto>> GetProducts()
    {
        var products = await _dbContext
            .Products
            .ToListAsync();
        
        return _mapper.Map<List<ProductDto>>(products);
    }

    public async Task<ProductDto> GetProductById(int productId)
    {
        var product = await _dbContext
            .Products
            .SingleAsync(product => product.ProductId == productId);
        
        return _mapper.Map<ProductDto>(product);
    }

    public async Task<ProductDto> CreateUpdateProduct(ProductDto productDto)
    {
        var product = _mapper.Map<Product>(productDto);

        if (product.ProductId > 0)
        {
            _dbContext.Products.Update(product);
        }
        else
        {
            _dbContext.Products.Add(product);
        }
        
        await _dbContext.SaveChangesAsync();

        return _mapper.Map<ProductDto>(product);
    }

    public async Task<bool> DeleteProduct(int productId)
    {
        try
        {
            var product = await _dbContext
                .Products
                .SingleAsync(product => product.ProductId == productId);

            _dbContext.Products.Remove(product);
            await _dbContext.SaveChangesAsync();
            
            return true;
        }
        //TODO: change to less common exception
        catch (Exception)
        {
            return false;
        }
    }
}
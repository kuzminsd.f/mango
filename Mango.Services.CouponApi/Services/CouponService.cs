﻿using AutoMapper;
using Mango.Infrastructure;
using Mango.Services.CouponApi.Models;
using Mango.Services.CouponApi.Models.Dto;
using Mango.Services.CouponApi.Repository;

namespace Mango.Services.CouponApi.Services;

public class CouponService
{
    private const string CouponNotFoundOrExpiredMessage = "Coupon does not exist or expired";
    
    private readonly ICouponRepository _couponRepository;
    private readonly ILogger<CouponService> _logger;
    private readonly IMapper _mapper;

    public CouponService(
        ILogger<CouponService> logger,
        ICouponRepository couponRepository,
        IMapper mapper)
    {
        _logger = logger;
        _couponRepository = couponRepository;
        _mapper = mapper;
    }
    
    public async Task<Response> GetCouponByCode(string couponCode, CancellationToken cancellationToken)
    {
        try
        {
            var coupon = await _couponRepository.GetByCode(couponCode, cancellationToken);
            var couponDto = _mapper.Map<CouponDto>(coupon);

            return new Response()
            {
                Result = couponDto,
                IsSuccess = true
            };

        }
        catch (Exception ex)
        {
            _logger.LogError("An error occured during getting coupon: {ex}", ex.Message);
            return new Response()
            {
                IsSuccess = false,
                DisplayMessage = "An error has occured. Please try later"
            };
        }
    }
    
    public async Task<Response> GetAll( CancellationToken cancellationToken)
    {
        try
        {
            var coupons = await _couponRepository.GetAll(cancellationToken);

            return new Response()
            {
                IsSuccess = true,
                Result = coupons
            };
        }
        catch (Exception ex)
        {
            _logger.LogError("An error occured during getting all coupons: {ex}", ex.Message);
            return new Response()
            {
                IsSuccess = false,
                DisplayMessage = "An error has occured. Please try later"
            };
        }
    }
    
    public async Task<Response> CreateCoupon(CouponDto coupon, CancellationToken cancellationToken)
    {
        try
        {
            var dbCoupon = _mapper.Map<Coupon>(coupon);
            var createdCoupon = await _couponRepository.Create(dbCoupon, cancellationToken);

            return new Response()
            {
                Result = createdCoupon,
                IsSuccess = true
            };
        }
        catch (Exception ex)
        {
            _logger.LogError("An error occured during creating coupon: {ex}", ex.Message);
            return new Response()
            {
                IsSuccess = false,
                DisplayMessage = "An error has occured. Please try later"
            };
        }
    }
    
    public async Task<Response> UpdateCoupon(CouponDto coupon, CancellationToken cancellationToken)
    {
        try
        {
            var storedCoupon = await _couponRepository.GetById(coupon.CouponId, cancellationToken);

            if (IsCouponValid(storedCoupon))
            {
                var couponForUpdate = _mapper.Map<Coupon>(coupon);

                var updatedCoupon = await _couponRepository.Update(couponForUpdate!, cancellationToken);
                
                return new Response()
                {
                    Result = updatedCoupon,
                    IsSuccess = true
                };
            }
            
            return new Response()
            {
                IsSuccess = false,
                DisplayMessage = CouponNotFoundOrExpiredMessage
            };
        }
        catch (Exception ex)
        {
            _logger.LogError("An error occured during updating coupon: {ex}", ex.Message);
            return new Response()
            {
                IsSuccess = false,
                DisplayMessage = "An error has occured. Please try later"
            };
        }
    }
    
    public async Task<Response> DeleteCoupon(int couponId, CancellationToken cancellationToken)
    {
        try
        {
            var coupon = await _couponRepository.GetById(couponId, cancellationToken);
            
            coupon!.IsDeleted = true;
            await _couponRepository.Update(coupon, cancellationToken);
            
            return new Response()
            {
                IsSuccess = true
            };
        }
        catch (Exception ex)
        {
            _logger.LogError("An error occured during removing coupon: {ex}", ex.Message);
            return new Response()
            {
                IsSuccess = false,
                DisplayMessage = "An error has occured. Please try later"
            };
        }
    }

    private bool IsCouponValid(Coupon? coupon)
    {
        return coupon is not null &&
               !coupon.IsDeleted &&
               coupon.ExpirationDate > DateTime.UtcNow;
    }
}
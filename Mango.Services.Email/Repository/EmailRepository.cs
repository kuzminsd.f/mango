﻿using Mango.Services.Email.Messages;
using Mango.Services.Email.Models;
using Mango.Services.OrderApi.DbContexts;

namespace Mango.Services.Email.Repository;

public class EmailRepository : IEmailRepository
{
    private readonly ApplicationDbContext _dbContext;

    public EmailRepository(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task SendAndLogEmail(UpdatePaymentResultMessage updatePaymentResultMessage)
    {
        //TODO: send a real email to client
        
        //Updating log in DB
        var logMessage = new EmailLog()
        {
            Email = updatePaymentResultMessage.Email ?? "undefined",
            EmailSent = DateTime.UtcNow,
            Log = $"Order {updatePaymentResultMessage.OrderId} has been created successfully"
        };

        await _dbContext.EmailLogs.AddAsync(logMessage);
        await _dbContext.SaveChangesAsync();
    }
}
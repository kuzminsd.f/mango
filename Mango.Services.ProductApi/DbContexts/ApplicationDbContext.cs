﻿using Microsoft.EntityFrameworkCore;
using Mango.Services.ProductApi.Models;

namespace Mango.Services.ProductApi.DbContexts;

public class ApplicationDbContext : DbContext
{
    public DbSet<Product> Products { get; set; } = default!;
    
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) 
        : base(options)
    {
        
    }
}
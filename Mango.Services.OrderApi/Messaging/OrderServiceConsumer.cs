﻿using Confluent.Kafka;
using Mango.MessageBus;
using Mango.Services.OrderApi.Messages;
using Mango.Services.OrderApi.Models;
using Mango.Services.OrderApi.Repository;

namespace Mango.Services.OrderApi.Messaging;

public class OrderServiceConsumer : BackgroundService
{
    private readonly ConsumerConfig _consumerConfig;
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<OrderServiceConsumer> _logger;
    private readonly IMessageBus _messageBus;

    public OrderServiceConsumer(
        IServiceProvider serviceProvider, 
        KafkaOptions kafkaOptions, 
        ILogger<OrderServiceConsumer> logger, IMessageBus messageBus)
    {
        _serviceProvider = serviceProvider;
        _consumerConfig = new ConsumerConfig
        {
            BootstrapServers = kafkaOptions.BootstrapServers,
            GroupId = "order-api-service",
            AutoOffsetReset = AutoOffsetReset.Earliest,
            MaxPollIntervalMs = 1 * 60 * 1000,
            AutoCommitIntervalMs = 5 * 1000,
            EnableAutoCommit = true,
            EnableAutoOffsetStore = false
        };
        _logger = logger;
        _messageBus = messageBus;
    }

    private async Task OnCheckOutMessageReceived(
        IOrderRepository orderRepository,
        CheckoutHeaderDto checkoutHeaderDto,
        CancellationToken stoppingToken)
    {
        var orderHeader = new OrderHeader()
        {
            UserId = checkoutHeaderDto.UserId,
            FirstName = checkoutHeaderDto.FirstName,
            LastName = checkoutHeaderDto.LastName,
            OrderDetails = new List<OrderDetails>(),
            CardNumber = checkoutHeaderDto.CardNumber,
            CouponCode = checkoutHeaderDto.CouponCode,
            Cvv = checkoutHeaderDto.Cvv,
            DiscountTotal = checkoutHeaderDto.DiscountTotal,
            Email = checkoutHeaderDto.Email,
            ExpiryMonthYear = checkoutHeaderDto.ExpiryMonthYear,
            OrderTime = DateTime.UtcNow,
            OrderTotal = checkoutHeaderDto.OrderTotal,
            PaymentStatus = false,
            Phone = checkoutHeaderDto.Phone,
            PickupDateTime = checkoutHeaderDto.PickupDateTime,
        };

        foreach (var cartDetail in checkoutHeaderDto.CartDetails!)
        {
            var orderDetail = new OrderDetails()
            {
                ProductId = cartDetail.ProductId,
                ProductName = cartDetail.Product.Name,
                Price = cartDetail.Product.Price,
                Count = cartDetail.Count
            };
            
            orderHeader.OrderDetails.Add(orderDetail);
            orderHeader.CartTotalItems += orderDetail.Count;
        }

        orderHeader.PickupDateTime = DateTime.SpecifyKind(orderHeader.PickupDateTime, DateTimeKind.Utc);
        orderHeader.OrderTime = DateTime.SpecifyKind(orderHeader.OrderTime, DateTimeKind.Utc);

        await orderRepository.AddOrder(orderHeader);

        var paymentRequestMessage = new PaymentRequestMessage()
        {
            Id = orderHeader.OrderHeaderId,
            Name = $"{orderHeader.FirstName} {orderHeader.LastName}",
            Email = orderHeader.Email,
            CardNumber = orderHeader.CardNumber,
            Cvv = orderHeader.Cvv,
            ExpiryMonthYear = orderHeader.ExpiryMonthYear,
            OrderId = orderHeader.OrderHeaderId,
            OrderTotal = orderHeader.OrderTotal
        };
        
        await _messageBus.PublishMessage(paymentRequestMessage, KafkaTopics.OrderPaymentProcessTopic);
    }

    
    private async Task UpdatePaymentResult(
        IOrderRepository orderRepository,
        UpdatePaymentResultMessage updatePaymentResultMessage,
        CancellationToken stoppingToken)
    {
        await orderRepository.UpdateOrderPaymentStatus(
            updatePaymentResultMessage.OrderId,
            updatePaymentResultMessage.Status);
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var checkoutMessageTask = Task.Factory.StartNew(
            async () =>
            {
                try
                {
                    using var consumer = new ConsumerBuilder<long, CheckoutHeaderDto>(_consumerConfig)
                        .SetValueDeserializer(new JsonSerializer<CheckoutHeaderDto>())
                        .Build();

                    consumer.Subscribe(KafkaTopics.CheckoutMessageTopic);

                    var scope = _serviceProvider.CreateScope();
                    var orderRepository = scope.ServiceProvider.GetService<IOrderRepository>()!;

                    while (consumer.Consume(stoppingToken) is { } result)
                    {
                        await OnCheckOutMessageReceived(orderRepository, result.Message.Value, stoppingToken);

                        consumer.StoreOffset(result);

                        _logger.LogInformation("{OrderId} received", result.Message.Key);
                    }

                    consumer.Close();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Exception occured: {ex}", ex.Message);
                }
            }, stoppingToken);

        var orderPaymentUpdateResultTask = Task.Factory.StartNew(
            async () =>
            {
                try
                {
                    using var consumer = new ConsumerBuilder<long, UpdatePaymentResultMessage>(_consumerConfig)
                        .SetValueDeserializer(new JsonSerializer<UpdatePaymentResultMessage>())
                        .Build();

                    consumer.Subscribe(KafkaTopics.OrderUpdatePaymentResultTopic);
                    
                    var scope = _serviceProvider.CreateScope();
                    var orderRepository = scope.ServiceProvider.GetService<IOrderRepository>()!;

                    while (consumer.Consume(stoppingToken) is { } result)
                    {
                        await UpdatePaymentResult(orderRepository, result.Message.Value, stoppingToken);

                        consumer.StoreOffset(result);

                        _logger.LogInformation("{OrderId} received", result.Message.Key);
                    }

                    consumer.Close();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Exception occured: {ex}", ex.Message);
                }
            }, stoppingToken);
        
        await Task.WhenAll(checkoutMessageTask, orderPaymentUpdateResultTask);
    }
}
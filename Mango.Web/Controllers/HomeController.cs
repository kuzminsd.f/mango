﻿using System.Diagnostics;
using System.Net;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using Mango.Web.Models;
using Mango.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Server.IISIntegration;
using Newtonsoft.Json;

namespace Mango.Web.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IProductService _productService;
    private readonly ICartService _cartService;

    public HomeController(
        ILogger<HomeController> logger, 
        IProductService productService, ICartService cartService)
    {
        _logger = logger;
        _productService = productService;
        _cartService = cartService;
    }

    public async Task<IActionResult> Index()
    {
        var products = new List<ProductDto>();
        var response = await _productService.GetAllProducts<ResponseDto>("");

        if (response.Result != null && response.IsSuccess)
        {
            products = JsonConvert.DeserializeObject<List<ProductDto>>(Convert.ToString(response.Result));
        }
        
        return View(products);
    }
    
    [Authorize]
    public async Task<IActionResult> Details(int productId)
    {
        var response = await _productService.GetProductById<ResponseDto>("", productId);
        
        if (response.Result != null && response.IsSuccess)
        {
            var product = JsonConvert.DeserializeObject<ProductDto>(Convert.ToString(response.Result));
            
            return View(product);
        }

        return NotFound();
    }
    
    [Authorize]
    [ActionName("Details")]
    [HttpPost]
    public async Task<IActionResult> Details(ProductDto productDto)
    {
        var cart = new CartDto()
        {
            CartHeader = new CartHeaderDto()
            {
                UserId = User.Claims
                    .FirstOrDefault(x => x.Type == "sub")?.Value
            }
        };

        var cartDetails = new CartDetailsDto()
        {
            Count = productDto.Count,
            ProductId = productDto.ProductId,
            Product = productDto
        };

        cart.CartDetails = new[] { cartDetails };

        var accessToken = await HttpContext.GetTokenAsync("access_token");
        var productResponse = await _productService.GetProductById<ResponseDto>(accessToken, productDto.ProductId);

        if (productResponse.IsSuccess && productResponse.Result != null)
        {
            var product = JsonConvert.DeserializeObject<ProductDto>(productResponse.Result.ToString());
            cartDetails.Product = product;
            cartDetails.CartHeader = cart.CartHeader;
            
            var response = await _cartService.AddToCart<ResponseDto>(accessToken, cart);

            if (response.Result != null && response.IsSuccess)
            {
                return RedirectToAction(nameof(Index));
            }
            
            //TODO: add information about problem to let user know something went wrong
            return View(product);
        }

        //TODO: add information about problem to let user know something went wrong
        return View(productDto);
    }

    [Authorize]
    public async Task<IActionResult> Login()
    {
        return RedirectToAction(nameof(Index));
    }
    
    public IActionResult Logout()
    {
        return SignOut("Cookies", "oidc");
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
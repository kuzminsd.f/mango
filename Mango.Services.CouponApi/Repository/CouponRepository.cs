﻿using Mango.Services.CouponApi.DbContexts;
using Mango.Services.CouponApi.Models;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.CouponApi.Repository;

public class CouponRepository : ICouponRepository
{
    private readonly ApplicationDbContext _dbContext;

    public CouponRepository(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<Coupon?> GetByCode(string couponCode, CancellationToken cancellationToken)
    {
        var coupon = await _dbContext
            .Coupons
            .FirstOrDefaultAsync(x => x.CouponCode == couponCode, cancellationToken);

        return coupon;
    }

    public async Task<Coupon?> GetById(int couponId, CancellationToken cancellationToken)
    {
        var coupon = await _dbContext
            .Coupons
            .AsNoTracking()
            .FirstOrDefaultAsync(x => x.CouponId == couponId, cancellationToken);

        return coupon;
    }

    public async Task<IEnumerable<Coupon>> GetAll(CancellationToken cancellationToken)
    {
        return await _dbContext.Coupons.ToListAsync(cancellationToken);
    }

    public async Task<Coupon> Create(Coupon coupon, CancellationToken cancellationToken)
    {
        _dbContext.Coupons.Add(coupon);
        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return coupon;
    }

    public async Task<Coupon> Update(Coupon coupon, CancellationToken cancellationToken)
    {
        _dbContext.Coupons.Update(coupon);
        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return coupon;
    }
}
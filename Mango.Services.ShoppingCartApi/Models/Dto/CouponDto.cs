﻿namespace Mango.Services.ShoppingCartApi.Models.Dto;

public class CouponDto
{
    public int CouponId { get; set; }
    public string CouponCode { get; set; } = default!;
    public double DiscountAmount { get; set; }
}
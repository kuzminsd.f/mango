# Readme

## About the library

This is a shared library that should be used across all projects.
It should contain only auxiliary classes and extension methods - there should be no business logic at all.
It may be ported to the Nuget store in the future.
﻿using System.ComponentModel.DataAnnotations;

namespace Mango.Services.CouponApi.Models;

public class Coupon
{
    [Key]
    public int CouponId { get; set; }

    public string CouponCode { get; set; } = default!;
    
    public double DiscountAmount { get; set; }
    
    public bool FixedAmount { get; set; }
    
    public DateTime ExpirationDate { get; set; }
    
    public bool IsDeleted { get; set; }
}
﻿using AutoMapper;
using Mango.Services.ShoppingCartApi.DbContexts;
using Mango.Services.ShoppingCartApi.Models;
using Mango.Services.ShoppingCartApi.Models.Dto;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.ShoppingCartApi.Repository;

public class CartRepository : ICartRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public CartRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    public async Task<CartDto> GetCartByUserId(string userId)
    {
        var cartHeaderFromDb = await _dbContext
            .CartHeaders
            .FirstAsync(x => x.UserId == userId);
        
        
        //TODO: replace with async enumerable
        var cartDetails = _dbContext
            .CartDetails
            .Where(x => x.CartHeaderId == cartHeaderFromDb.CartHeaderId)
            .Include(x => x.Product);

        var cart = new Cart()
        {
            CartHeader = cartHeaderFromDb,
            CartDetails = cartDetails
        };

        return _mapper.Map<CartDto>(cart);
    }
    
    public async Task<CartDto> CreateUpdateCart(CartDto cartDto)
    {
        var cart = _mapper.Map<Cart>(cartDto);
        var cartDetails = cart.CartDetails.First();
        var product = await _dbContext
            .Products
            .FirstOrDefaultAsync(x => x.ProductId == cartDetails.ProductId);

        if (product is null)
        {
            _dbContext.Products.Add(cartDetails.Product);
            await _dbContext.SaveChangesAsync();
        }

        var cartHeaderFromDb = await _dbContext
            .CartHeaders
            .AsNoTracking()
            .FirstOrDefaultAsync(x => x.UserId == cart.CartHeader.UserId);

        if (cartHeaderFromDb is null)
        {
            _dbContext.CartHeaders.Add(cart.CartHeader);
            //TODO: check if we should save changes each time we made them
            await _dbContext.SaveChangesAsync();
            cartDetails.CartHeaderId = cart.CartHeader.CartHeaderId;

            //TODO: check how we can avoid this dirty hack
            cartDetails.Product = null;
            cartDetails.CartHeader = null;
            _dbContext.CartDetails.Add(cartDetails);
            await _dbContext.SaveChangesAsync();
        }
        else
        {
            var cartDetailsFromDb = await _dbContext
                    .CartDetails
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.CartHeaderId == cartHeaderFromDb.CartHeaderId &&
                                     x.ProductId == cartDetails.ProductId);

            if (cartDetailsFromDb is null)
            {
                cartDetails.CartHeaderId = cartHeaderFromDb.CartHeaderId;
                cartDetails.Product = null;
                cartDetails.CartHeader = null;
                
                _dbContext.CartDetails.Add(cartDetails);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                cartDetails.Count += cartDetailsFromDb.Count;
                cartDetails.Product = null;
                cartDetails.CartHeader = null;
                cartDetails.CartHeaderId = cartDetailsFromDb.CartHeaderId;
                cartDetails.CartDetailsId = cartDetailsFromDb.CartDetailsId;
                _dbContext.CartDetails.Update(cartDetails);

                await _dbContext.SaveChangesAsync();
            }
        }

        return _mapper.Map<CartDto>(cart);
    }

    public async Task<bool> RemoveFromCart(int cartDetailsId)
    {
        var cartDetails = await _dbContext
            .CartDetails
            .FirstOrDefaultAsync(x => x.CartDetailsId == cartDetailsId);

        if (cartDetails is not null)
        {
            //TODO: try to check after removing
            var totalCountOfCartItems = _dbContext
                .CartDetails
                .Count(x => x.CartHeaderId == cartDetails.CartHeaderId);
            
            _dbContext.Remove(cartDetails);

            if (totalCountOfCartItems == 1)
            {
                var cartHeader = await _dbContext
                    .CartHeaders
                    .FirstAsync(x => x.CartHeaderId == cartDetails.CartHeaderId);

                _dbContext.CartHeaders.Remove(cartHeader);
            }

            await _dbContext.SaveChangesAsync();

            return true;
        }

        return false;
    }

    public async Task<bool> ApplyCoupon(string userId, string couponCode)
    {
        var cartHeaderFromDb = await _dbContext
            .CartHeaders
            .FirstOrDefaultAsync(x => x.UserId == userId);

        if (cartHeaderFromDb is not null)
        {
            cartHeaderFromDb.CouponCode = couponCode;
            _dbContext.CartHeaders.Update(cartHeaderFromDb);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        return false;
    }

    public async Task<bool> RemoveCoupon(string userId)
    {
        var cartHeaderFromDb = await _dbContext
            .CartHeaders
            .FirstOrDefaultAsync(x => x.UserId == userId);

        if (cartHeaderFromDb is not null)
        {
            cartHeaderFromDb.CouponCode = null;
            _dbContext.CartHeaders.Update(cartHeaderFromDb);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        return false;
    }

    public async Task<bool> ClearCart(string userId)
    {
        var cartHeaderFromDb = await _dbContext
            .CartHeaders
            .FirstOrDefaultAsync(x => x.UserId == userId);

        if (cartHeaderFromDb is not null)
        {
            _dbContext
                .CartDetails
                .RemoveRange(
                _dbContext
                        .CartDetails
                        .Where(x => x.CartHeaderId == cartHeaderFromDb.CartHeaderId));
            
            _dbContext.CartHeaders.Remove(cartHeaderFromDb);

            await _dbContext.SaveChangesAsync();
            return true;
        }

        return false;
    }
}
﻿namespace Mango.Services.OrderApi;

public static class KafkaTopics
{
    public const string CheckoutMessageTopic  = "checkout_message";
    public const string OrderPaymentProcessTopic = "order_payment_process";
    public const string OrderUpdatePaymentResultTopic = "order_update_payment_result";

}
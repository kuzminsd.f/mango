﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Mango.Services.ProductApi.Models.Dto;
using Mango.Services.ProductApi.Repository;
using Microsoft.AspNetCore.Authorization;

namespace Mango.Services.ProductApi.Controllers;

[Route("api/products")]
public class ProductApiController : ControllerBase
{
    private IProductRepository _productRepository;

    public ProductApiController(IProductRepository productRepository)
    {
        _productRepository = productRepository;
    }

    [HttpGet]
    public async Task<ResponseDto> Get()
    {
        var response = new ResponseDto();
        
        try
        {
            var products = await _productRepository.GetProducts();
            response.Result = products;
        }
        catch(Exception ex)
        {
            response.IsSuccess = false;
            response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return response;
    }

    [HttpGet]
    [Route("{id}")]
    public async Task<ResponseDto> Get(int id)
    {
        var response = new ResponseDto();
        
        try
        {
            var product = await _productRepository.GetProductById(id);
            response.Result = product;
        }
        catch(Exception ex)
        {
            response.IsSuccess = false;
            response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return response;
    }
    
    [Authorize]
    [HttpPost]
    public async Task<ResponseDto> Create([FromBody]ProductDto product)
    {
        var response = new ResponseDto();
        
        try
        {
            var createdProduct = await _productRepository.CreateUpdateProduct(product);
            response.Result = createdProduct;
        }
        catch(Exception ex)
        {
            response.IsSuccess = false;
            response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return response;
    }
    
    [Authorize]
    [HttpPut]
    public async Task<ResponseDto> Update([FromBody]ProductDto product)
    {
        var response = new ResponseDto();
        
        try
        {
            var updatedProduct = await _productRepository.CreateUpdateProduct(product);
            response.Result = updatedProduct;
        }
        catch(Exception ex)
        {
            response.IsSuccess = false;
            response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return response;
    }
    
    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Route("{id}")]
    public async Task<ResponseDto> Delete(int id)
    {
        var response = new ResponseDto();
        
        try
        {
            var deletingSuccess = await _productRepository.DeleteProduct(id);
            response.Result = deletingSuccess;
        }
        catch(Exception ex)
        {
            response.IsSuccess = false;
            response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return response;
    }
}
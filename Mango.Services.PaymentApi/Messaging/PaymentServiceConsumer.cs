﻿using Confluent.Kafka;
using Mango.MessageBus;
using Mango.Services.PaymentApi.Messages;
using PaymentProcessor;

namespace Mango.Services.PaymentApi.Messaging;

public class OrderServiceConsumer : BackgroundService
{
    private readonly ConsumerConfig _consumerConfig;
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<OrderServiceConsumer> _logger;
    private readonly IMessageBus _messageBus;
    private readonly IProcessPayment _processPayment;
    
    public OrderServiceConsumer(
        IServiceProvider serviceProvider, 
        KafkaOptions kafkaOptions, 
        ILogger<OrderServiceConsumer> logger, IMessageBus messageBus,
        IProcessPayment processPayment)
    {
        _serviceProvider = serviceProvider;
        _consumerConfig = new ConsumerConfig
        {
            BootstrapServers = kafkaOptions.BootstrapServers,
            GroupId = "payment-api-service",
            AutoOffsetReset = AutoOffsetReset.Earliest,
            MaxPollIntervalMs = 1 * 60 * 1000,
            AutoCommitIntervalMs = 5 * 1000,
            EnableAutoCommit = true,
            EnableAutoOffsetStore = false
        };
        _logger = logger;
        _messageBus = messageBus;
        _processPayment = processPayment;
    }

    /*private async Task OnCheckOutMessageReceived(
        IOrderRepository orderRepository,
        CheckoutHeaderDto checkoutHeaderDto,
        CancellationToken stoppingToken)
    {
        var orderHeader = new OrderHeader()
        {
            UserId = checkoutHeaderDto.UserId,
            FirstName = checkoutHeaderDto.FirstName,
            LastName = checkoutHeaderDto.LastName,
            OrderDetails = new List<OrderDetails>(),
            CardNumber = checkoutHeaderDto.CardNumber,
            CouponCode = checkoutHeaderDto.CouponCode,
            Cvv = checkoutHeaderDto.Cvv,
            DiscountTotal = checkoutHeaderDto.DiscountTotal,
            Email = checkoutHeaderDto.Email,
            ExpiryMonthYear = checkoutHeaderDto.ExpiryMonthYear,
            OrderTime = DateTime.UtcNow,
            OrderTotal = checkoutHeaderDto.OrderTotal,
            PaymentStatus = false,
            Phone = checkoutHeaderDto.Phone,
            PickupDateTime = checkoutHeaderDto.PickupDateTime,
        };

        foreach (var cartDetail in checkoutHeaderDto.CartDetails!)
        {
            var orderDetail = new OrderDetails()
            {
                ProductId = cartDetail.ProductId,
                ProductName = cartDetail.Product.Name,
                Price = cartDetail.Product.Price,
                Count = cartDetail.Count
            };
            
            orderHeader.OrderDetails.Add(orderDetail);
            orderHeader.CartTotalItems += orderDetail.Count;
        }

        orderHeader.PickupDateTime = DateTime.SpecifyKind(orderHeader.PickupDateTime, DateTimeKind.Utc);
        orderHeader.OrderTime = DateTime.SpecifyKind(orderHeader.OrderTime, DateTimeKind.Utc);

        await orderRepository.AddOrder(orderHeader);

        var paymentRequestMessage = new PaymentRequestMessage()
        {
            Name = $"{orderHeader.FirstName} {orderHeader.LastName}",
            CardNumber = orderHeader.CardNumber,
            Cvv = orderHeader.Cvv,
            ExpiryMonthYear = orderHeader.ExpiryMonthYear,
            OrderId = orderHeader.OrderHeaderId,
            OrderTotal = orderHeader.OrderTotal
        };
        
        await _messageBus.PublishMessage(paymentRequestMessage, KafkaTopics.OrderPaymentProcessTopic);
    }*/

    private async Task ProcessPayment(
        PaymentRequestMessage paymentRequestMessage, 
        CancellationToken stoppingToken)
    {
        var paymentStatus = _processPayment.PaymentProcessor();
        var updatePaymentResultMessage = new UpdatePaymentResultMessage()
        {
            Id = paymentRequestMessage.OrderId,
            OrderId = paymentRequestMessage.OrderId,
            Status = paymentStatus,
            Email = paymentRequestMessage.Email
        };

        await _messageBus.PublishMessage(updatePaymentResultMessage, KafkaTopics.OrderUpdatePaymentResultTopic);
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var task = Task.Factory.StartNew(
            async () =>
            {
                try
                {
                    using var consumer = new ConsumerBuilder<long, PaymentRequestMessage>(_consumerConfig)
                        .SetValueDeserializer(new JsonSerializer<PaymentRequestMessage>())
                        .Build();

                    consumer.Subscribe(KafkaTopics.OrderPaymentProcessTopic);

                    while (consumer.Consume(stoppingToken) is { } result)
                    {
                        await ProcessPayment(result.Message.Value, stoppingToken);

                        consumer.StoreOffset(result);

                        _logger.LogInformation("{OrderId} received", result.Message.Key);
                    }

                    consumer.Close();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Exception occured: {ex}", ex.Message);
                }
            }, stoppingToken);

        await task.WaitAsync(stoppingToken);
    }
}
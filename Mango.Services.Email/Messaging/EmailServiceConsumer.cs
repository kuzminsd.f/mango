﻿using Confluent.Kafka;
using Mango.MessageBus;
using Mango.Services.Email.Messages;
using Mango.Services.Email.Repository;
using Mango.Services.OrderApi;

namespace Mango.Services.Email.Messaging
{
    public class EmailServiceConsumer : BackgroundService
    {
        private readonly ConsumerConfig _consumerConfig;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<EmailServiceConsumer> _logger;

        public EmailServiceConsumer(
            IServiceProvider serviceProvider, 
            KafkaOptions kafkaOptions, 
            ILogger<EmailServiceConsumer> logger)
        {
            _serviceProvider = serviceProvider;
            _consumerConfig = new ConsumerConfig
            {
                BootstrapServers = kafkaOptions.BootstrapServers,
                GroupId = "email-api-service",
                AutoOffsetReset = AutoOffsetReset.Earliest,
                MaxPollIntervalMs = 1 * 60 * 1000,
                AutoCommitIntervalMs = 5 * 1000,
                EnableAutoCommit = true,
                EnableAutoOffsetStore = false
            };
            _logger = logger;
        }

        private async Task UpdatePaymentResult(
            IEmailRepository emailRepository,
            UpdatePaymentResultMessage updatePaymentResultMessage,
            CancellationToken stoppingToken)
        {
            await emailRepository.SendAndLogEmail(updatePaymentResultMessage);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var orderPaymentUpdateResultTask = Task.Factory.StartNew(
                async () =>
                {
                    try
                    {
                        using var consumer = new ConsumerBuilder<long, UpdatePaymentResultMessage>(_consumerConfig)
                            .SetValueDeserializer(new JsonSerializer<UpdatePaymentResultMessage>())
                            .Build();

                        consumer.Subscribe(KafkaTopics.OrderUpdatePaymentResultTopic);
                    
                        var scope = _serviceProvider.CreateScope();
                        var orderRepository = scope.ServiceProvider.GetService<IEmailRepository>()!;

                        while (consumer.Consume(stoppingToken) is { } result)
                        {
                            await UpdatePaymentResult(orderRepository, result.Message.Value, stoppingToken);

                            consumer.StoreOffset(result);

                            _logger.LogInformation("{OrderId} received", result.Message.Key);
                        }

                        consumer.Close();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("Exception occured: {ex}", ex.Message);
                    }
                }, stoppingToken);
        
            await orderPaymentUpdateResultTask.WaitAsync(stoppingToken);
        }
    }
}
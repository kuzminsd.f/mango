﻿using Mango.Web.Models;
using Mango.Web.Services.Interfaces;

namespace Mango.Web.Services;

public class CouponService : BaseService, ICouponService
{
    public CouponService(IHttpClientFactory httpClient ) : base(httpClient)
    {
        
    }

    public async Task<T> GetCoupon<T>(string token, string couponCode)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Get,
            Url = $"{Settings.CouponApiBase}/api/coupon/{couponCode}",
            AccessToken = token
        });

        return result;
    }
}
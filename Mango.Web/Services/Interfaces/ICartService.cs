﻿using Mango.Web.Models;

namespace Mango.Web.Services.Interfaces;

public interface ICartService
{
    Task<T> GetCartByUserId<T>(string token, string userId);
    Task<T> AddToCart<T>(string token, CartDto cartDto);
    Task<T> UpdateCart<T>(string token, CartDto cartDto);
    Task<T> RemoveFromCart<T>(string token, int cartId);
    Task<T> ApplyCoupon<T>(string token, CartDto cartDto);
    Task<T> RemoveCoupon<T>(string token, string userId);
    Task<T> Checkout<T>(string token, CartHeaderDto cartHeader);

}
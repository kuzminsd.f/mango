﻿using Mango.Web.Models;
using Mango.Web.Services.Interfaces;

namespace Mango.Web.Services;

public class CartService : BaseService, ICartService
{
    public CartService(IHttpClientFactory httpClient) : base(httpClient)
    {
    }

    public async Task<T> GetCartByUserId<T>(string token, string userId)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Get,
            Url = $"{Settings.ShoppingCartApiBase}/api/cart/GetCart/{userId}",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> AddToCart<T>(string token, CartDto cartDto)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Post,
            Data = cartDto,
            Url = $"{Settings.ShoppingCartApiBase}/api/cart/AddCart",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> UpdateCart<T>(string token, CartDto cartDto)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Post,
            Data = cartDto,
            Url = $"{Settings.ShoppingCartApiBase}/api/cart/UpdateCart",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> RemoveFromCart<T>(string token, int cartId)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Post,
            Data = cartId,
            Url = $"{Settings.ShoppingCartApiBase}/api/cart/RemoveCart",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> ApplyCoupon<T>(string token, CartDto cartDto)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Post,
            Data = cartDto,
            Url = $"{Settings.ShoppingCartApiBase}/api/cart/ApplyCoupon",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> RemoveCoupon<T>(string token, string userId)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Post,
            Data = userId,
            Url = $"{Settings.ShoppingCartApiBase}/api/cart/RemoveCoupon",
            AccessToken = token
        });

        return result;
    }

    public async Task<T> Checkout<T>(string token, CartHeaderDto cartHeader)
    {
        var result = await this.Send<T>(new ApiRequest()
        {
            ApiType = Settings.ApiType.Post,
            Data = cartHeader,
            Url = $"{Settings.ShoppingCartApiBase}/api/cart/checkout",
            AccessToken = token
        });

        return result;
    }
}
﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mango.Web.Models;

public class ProductDto
{
    public int ProductId { get; set; }
    
    public string Name { get; set; } = default!;
    
    public double Price { get; set; }
    
    public string? Description { get; set; }
    
    [DisplayName("Category Name")]
    public string CategoryName { get; set; } = default!;
    
    public string? ImageUrl { get; set; }

    [Range(1, 100)]
    public int Count { get; set; } = 1;
}
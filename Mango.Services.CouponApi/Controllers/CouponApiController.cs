﻿using Mango.Infrastructure;
using Mango.Services.CouponApi.Models.Dto;
using Mango.Services.CouponApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Mango.Services.CouponApi.Controllers;

[ApiController]
[Authorize]
[Route("api/coupon")]
public class CouponApiController : Controller
{
    private const string ContentMakerRole = "ContentMaker";
    private const string SuperUserRole = "Admin";
    private readonly CouponService _couponService;
    
    public CouponApiController(CouponService couponService)
    {
        _couponService = couponService;
    }
    
    [HttpGet("{couponCode}")]
    public async Task<Response> GetCouponByCode(string couponCode, CancellationToken cancellationToken)
    {
        return await _couponService.GetCouponByCode(couponCode, cancellationToken);
    }
    
    [HttpGet]
    [Authorize(Roles = $"{ContentMakerRole}, {SuperUserRole}")]
    public async Task<Response> GetAllCoupons(CancellationToken cancellationToken)
    {
        return await _couponService.GetAll(cancellationToken);
    }
    
    [HttpPost]
    [Authorize(Roles = $"{ContentMakerRole}, {SuperUserRole}")]
    public async Task<Response> Create(CouponDto coupon, CancellationToken cancellationToken)
    {
        return await _couponService.CreateCoupon(coupon, cancellationToken);
    }
    
    [HttpPut]
    [Authorize(Roles = $"{ContentMakerRole}, {SuperUserRole}")]
    public async Task<Response> Update(CouponDto coupon, CancellationToken cancellationToken)
    {
        return await _couponService.UpdateCoupon(coupon, cancellationToken);
    }
    
    [HttpDelete("{couponId}")]
    [Authorize(Roles = $"{ContentMakerRole}, {SuperUserRole}")]
    public async Task<Response> Delete(int couponId, CancellationToken cancellationToken)
    {
        return await _couponService.DeleteCoupon(couponId, cancellationToken);
    }
}
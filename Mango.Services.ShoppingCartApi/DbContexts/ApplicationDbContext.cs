﻿using Mango.Services.ShoppingCartApi.Models;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.ShoppingCartApi.DbContexts;

public class ApplicationDbContext : DbContext
{
    public DbSet<Product> Products { get; set; } = default!;
    
    public DbSet<CartHeader> CartHeaders { get; set; } = default!;

    public DbSet<CartDetails> CartDetails { get; set; } = default!;

    
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) 
        : base(options)
    {
        
    }
}
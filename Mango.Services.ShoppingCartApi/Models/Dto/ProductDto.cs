﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mango.Services.ShoppingCartApi.Models.Dto;

public class ProductDto
{
    public int ProductId { get; set; }

    public string Name { get; set; } = default!;

    public double Price { get; set; }
    
    public string? Description { get; set; }

    public string CategoryName { get; set; } = default!;
    
    public string? ImageUrl { get; set; }
}
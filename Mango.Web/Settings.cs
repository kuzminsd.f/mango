﻿namespace Mango.Web;

public static class Settings
{
    public enum ApiType
    {
        Get,
        Post,
        Put,
        Delete
    }

    public static string ProductApiBase { get; set; } = default!;
    public static string ShoppingCartApiBase { get; set; } = default!;
    public static string CouponApiBase { get; set; } = default!;


}
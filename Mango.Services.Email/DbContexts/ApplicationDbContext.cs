﻿using Mango.Services.Email.Models;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.OrderApi.DbContexts;

public class ApplicationDbContext : DbContext
{
    public DbSet<EmailLog> EmailLogs { get; set; } = default!;

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) 
        : base(options)
    {
        
    }
}